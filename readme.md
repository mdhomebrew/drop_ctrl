Proof of Concept showing that an off-the-shelf reprogrammable keyboard can be used to rapidly inject payloads onto a computer.

Adds basic payload injection to keyboard using QMK send string at a high rate of speed into a powershell window. Payload draws a red box over a CounterStrike Global Offensive window when alt+h is pressed.
Build and flash using QMK MSYS and QMK Toolbox

keymap.c goes in keyboards\massdrop\ctrl\keymaps\<yourkeymap>\keymap.c
config.h and rules.mk overwrite the same files in keyboards\massdrop\ctrl\

Press Fn + Right Ctrl to have the keyboard type the following into a notepad window:

	Welcome to your new utility keyboard!
	QMK is an open source keyboard firmware that allows you
	to set your own customized keyboard layouts and so much more!
	
	Commands        :
		Fn + Right Alt  : Inject CS:GO Overlay Program onto computer discretely (small resized notepad)
		Fn + App        : Inject CS:GO Overlay Program computer non-discretely (no resizing of notepad)
		Fn + Right Ctrl : Type out a readme in notepad.exe
		Alt + h         : Once the program has been injected, use Alt+h to enable/disable the Box Overlay while CS:GO is running" 
	
	Other commands  :
		Fm + Left Ctrl  : Reset Keyboard (Enter programming mode)

	Thanks for playing, have fun!