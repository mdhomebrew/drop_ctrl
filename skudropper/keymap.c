#include QMK_KEYBOARD_H
#include "pointing_device.h"

enum ctrl_keycodes {
    U_T_AUTO = SAFE_RANGE, //USB Extra Port Toggle Auto Detect / Always Active
    U_T_AGCR,              //USB Toggle Automatic GCR control
    DBG_TOG,               //DEBUG Toggle On / Off
    DBG_MTRX,              //DEBUG Toggle Matrix Prints
    DBG_KBD,               //DEBUG Toggle Keyboard Prints
    DBG_MOU,               //DEBUG Toggle Mouse Prints
    MD_BOOT,               //Restart into bootloader after hold timeout
	HAK,                   //CS:GO Box Drawer
	HAK_VIS,               //CS:GO Box Drawer visible entry
	README                 //Type a readme in notepad.exe
};

keymap_config_t keymap_config;

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [0] = LAYOUT(
        KC_ESC,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,             KC_PSCR, KC_SLCK, KC_PAUS, \
        KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC,   KC_INS,  KC_HOME, KC_PGUP, \
        KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC, KC_BSLS,   KC_DEL,  KC_END,  KC_PGDN, \
        KC_CAPS, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT, KC_ENT, \
        KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT,                              KC_UP, \
        KC_LCTL, KC_LGUI, KC_LALT,                   KC_SPC,                             KC_RALT, MO(1),   KC_APP,  KC_RCTL,            KC_LEFT, KC_DOWN, KC_RGHT \
    ),
    [1] = LAYOUT(
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,            KC_MUTE, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,   KC_MPLY, KC_MSTP, KC_VOLU, \
        _______, RGB_SPD, RGB_VAI, RGB_SPI, RGB_HUI, RGB_SAI, _______, U_T_AUTO,U_T_AGCR,_______, _______, _______, _______, _______,   KC_MPRV, KC_MNXT, KC_VOLD, \
        _______, RGB_RMOD,RGB_VAD, RGB_MOD, RGB_HUD, RGB_SAD, _______, _______, _______, _______, _______, _______, _______, \
        _______, RGB_TOG, _______, _______, _______, MD_BOOT, NK_TOGG, _______, _______, _______, _______, _______,                              _______, \
        RESET  , _______, _______,                   _______,                            HAK    , _______, HAK_VIS, README ,            _______, _______, _______ \
    ),
    /*
    [X] = LAYOUT(
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,            _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,   _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,   _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        _______, _______, _______, _______, _______, _______, NK_TOGG, _______, _______, _______, _______, _______,                              _______, \
        _______, _______, _______,                   _______,                            _______, _______, _______, _______,            _______, _______, _______ \
    ),
    */
};



// Runs just one time when the keyboard initializes.
void matrix_init_user(void) {
};

// Runs constantly in the background, in a loop.
void matrix_scan_user(void) {
};

// Function Prototypes
void payload(void);

void readme(void);

void dumptexthidden(void);

void dumptextvisible(void);

void releaseme(void);

#define MODS_SHIFT  (get_mods() & MOD_MASK_SHIFT)
#define MODS_CTRL   (get_mods() & MOD_MASK_CTRL)
#define MODS_ALT    (get_mods() & MOD_MASK_ALT)

void payload(void) {	
	// Program to draw box over CS:GO
	SEND_STRING("$code = \"using System; using System.Threading; using System.Runtime.InteropServices;   namespace DrawBox { public class Program { [StructLayout(LayoutKind.Sequential)] public struct POINT { public int X; public int Y; } [StructLayout(LayoutKind.Sequential)] public struct MSG { public IntPtr hwnd; public UInt32 message; public IntPtr wParam; public IntPtr lParam; public UInt32 time; public POINT pt; }  [DllImport(`\"User32.dll`\")] public static extern IntPtr GetDC(IntPtr hwnd); [DllImport(`\"User32.dll`\")] public static extern void ReleaseDC(IntPtr hwnd, IntPtr dc); [DllImport(`\"Gdi32.dll`\")] public static extern UInt32 SetPixel(IntPtr hdc, int x, int y, UInt32 color); [DllImport(`\"User32.dll`\", EntryPoint = `\"FindWindow`\", SetLastError = true)] public static extern IntPtr FindWindowByName(IntPtr ZeroOnly, string window_name); [DllImport(`\"User32.dll`\", SetLastError = true)] [return: MarshalAs(UnmanagedType.Bool)] static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk); [DllImport(`\"User32.dll`\")] static extern int GetMessage(out MSG lpMsg, IntPtr hWnd, uint wMsgFilterMin, uint wMsgFilterMax);  public static bool g_draw = false; public static IntPtr g_hdc;  public static void DrawBox() { while (true) { if(g_draw) { int x = 20; int y = 20; for (x = 20; x < 40; x++) { for (y = 20; y < 40; y++) { SetPixel(g_hdc, x, y, 0x000000FF); } } } } }  public static void Main(string[] args) { MSG msg; uint u_hhot = 0x48; RegisterHotKey(IntPtr.Zero, 1, 1, u_hhot);  Thread draw_thread = new Thread(DrawBox); draw_thread.Start();  while (GetMessage(out msg, IntPtr.Zero, 0, 0) != 0) { if (msg.message == 0x0312) { UInt16 key_code = (UInt16)((uint)msg.lParam >> 16); if(key_code == 0x48) { if(g_draw == false) { IntPtr hwnd; hwnd = FindWindowByName(IntPtr.Zero, `\"Counter-Strike: Global Offensive - Direct3D 9`\"); if (hwnd == IntPtr.Zero) { Console.WriteLine(`\"Window not found!`\"); continue; } g_hdc = GetDC(hwnd); if(g_hdc == IntPtr.Zero) { continue; } g_draw = true; } else { g_draw = false; } } } } } } }\"");
	SEND_STRING(SS_DELAY(100) SS_TAP(X_ENTER) SS_DELAY(100));
	
	// Book-keeping environment	
	SEND_STRING("Add-Type -TypeDefinition $code -Language CSharp");
	SEND_STRING(SS_DELAY(100) SS_TAP(X_ENTER) SS_DELAY(100));
	
	// Execute	
	SEND_STRING("iex \"[DrawBox.Program]::Main(0)\"");
	SEND_STRING(SS_DELAY(100) SS_TAP(X_ENTER) SS_DELAY(100));
	
	// Alt+Tab back to CS:GO
	SEND_STRING(SS_DOWN(X_LALT) SS_DELAY(100) SS_TAP(X_TAB) SS_DELAY(100) SS_UP(X_LALT));
}

void readme(void) {		
	SEND_STRING(SS_TAP(X_LWIN) SS_DELAY(100) "notepad.exe %temp%/hackeyboardreadme.txt" SS_DELAY(100) SS_TAP(X_ENT) SS_DELAY(700) SS_TAP(X_ENT) SS_DELAY(700));
	SEND_STRING("Welcome to your new utility keyboard!" SS_TAP(X_ENT) 
				"   QMK is an open source keyboard firmware that allows you" SS_TAP(X_ENT) 
				"   to set your own customized keyboard layouts and so much more!" SS_TAP(X_ENT) 
				SS_TAP(X_ENT) 
	            "Commands        :" SS_TAP(X_ENT) 
				"Fn + Right Alt  : Inject CS:GO Overlay Program onto computer discretely (small resized notepad)" SS_TAP(X_ENT) 
				"Fn + App        : Inject CS:GO Overlay Program computer non-discretely (no resizing of notepad)" SS_TAP(X_ENT) 
				"Fn + Right Ctrl : Type out a readme in notepad.exe" SS_TAP(X_ENT) 
				"Alt + h         : Once the program has been injected, use Alt+h to enable/disable the Box Overlay while CS:GO is running" SS_TAP(X_ENT)
			    SS_TAP(X_ENT)
				"Other commands  :" SS_TAP(X_ENT)
				"Fm + Left Ctrl  : Reset Keyboard (Enter programming mode)" SS_TAP(X_ENT)
				SS_TAP(X_ENT)
				SS_TAP(X_ENT)
				"Thanks for playing, have fun!" SS_TAP(X_ENT)
				);		
				
	SEND_STRING(SS_DOWN(X_LCTRL) SS_DELAY(300) "s" SS_DELAY(300) SS_UP(X_LCTRL) SS_DELAY(200));
}

// Dump powershell after en-smallening the window
void dumptexthidden(void) {	
	SEND_STRING(SS_TAP(X_LWIN) SS_DELAY(400) "powershell -noexit -command \"[console]::windowwidth=1; [console]::windowheight=1; [console]::bufferwidth=[console]::windowwidth\"" SS_DELAY(700) SS_TAP(X_ENT) SS_DELAY(500) );
	
	// Resize Window part 1 - grab lower right corner
	SEND_STRING(SS_DOWN(X_LALT) " " SS_DELAY(100) SS_UP(X_LALT) SS_DELAY(100) "s" SS_TAP(X_DOWN) SS_TAP(X_RIGHT));
	
	// Resize Window part 2 - move mouse up and left
	report_mouse_t currentReport;
	for(int i=0; i<20; i++) {
		currentReport = pointing_device_get_report();
		currentReport.x = -127; // positive to right
		currentReport.y = -127;  // positive up	
		pointing_device_set_report(currentReport);
		pointing_device_send();
		//SEND_STRING(SS_DELAY(100));		
	}
	
	// Resize Window part 3 - tap enter to accept resize
	SEND_STRING(SS_DELAY(100) SS_TAP(X_ENTER) SS_DELAY(100));	
    
	// Deliver Payload
	payload();
	
	// In case something screws up during development, release all keys
	clear_keyboard();
}

// Dump powershell without en-smallening the window
void dumptextvisible(void) {	
	SEND_STRING(SS_TAP(X_LWIN) SS_DELAY(400) "powershell" SS_DELAY(700) SS_TAP(X_ENT) SS_DELAY(500) );
	
	// Deliver Payload
	payload();

	// In case something screws up during development, release all keys
	clear_keyboard();
}

// Cleanup
void releaseme(void) {
	clear_keyboard();
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    static uint32_t key_timer;

    switch (keycode) {
		case HAK:
			if (record->event.pressed) {
				dumptexthidden();
			}
			return false;

		break;
		
		case HAK_VIS:
			if (record->event.pressed) {
				dumptextvisible();
			}
			return false;
			
		case README:
			if (record->event.pressed) {
				readme();
			}
			return false;

		break;
		case U_T_AUTO:
            if (record->event.pressed && MODS_SHIFT && MODS_CTRL) {
                TOGGLE_FLAG_AND_PRINT(usb_extra_manual, "USB extra port manual mode");
            }
            return false;
        case U_T_AGCR:
            if (record->event.pressed && MODS_SHIFT && MODS_CTRL) {
                TOGGLE_FLAG_AND_PRINT(usb_gcr_auto, "USB GCR auto mode");
            }
            return false;
        case DBG_TOG:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_enable, "Debug mode");
            }
            return false;
        case DBG_MTRX:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_matrix, "Debug matrix");
            }
            return false;
        case DBG_KBD:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_keyboard, "Debug keyboard");
            }
            return false;
        case DBG_MOU:
            if (record->event.pressed) {
                TOGGLE_FLAG_AND_PRINT(debug_mouse, "Debug mouse");
            }
            return false;
        case MD_BOOT:
            if (record->event.pressed) {
                key_timer = timer_read32();
            } else {
                if (timer_elapsed32(key_timer) >= 500) {
                    reset_keyboard();
                }
            }
            return false;
        case RGB_TOG:
            if (record->event.pressed) {
              switch (rgb_matrix_get_flags()) {
                case LED_FLAG_ALL: {
                    rgb_matrix_set_flags(LED_FLAG_KEYLIGHT | LED_FLAG_MODIFIER | LED_FLAG_INDICATOR);
                    rgb_matrix_set_color_all(0, 0, 0);
                  }
                  break;
                case (LED_FLAG_KEYLIGHT | LED_FLAG_MODIFIER | LED_FLAG_INDICATOR): {
                    rgb_matrix_set_flags(LED_FLAG_UNDERGLOW);
                    rgb_matrix_set_color_all(0, 0, 0);
                  }
                  break;
                case LED_FLAG_UNDERGLOW: {
                    rgb_matrix_set_flags(LED_FLAG_NONE);
                    rgb_matrix_disable_noeeprom();
                  }
                  break;
                default: {
                    rgb_matrix_set_flags(LED_FLAG_ALL);
                    rgb_matrix_enable_noeeprom();
                  }
                  break;
              }
            }
            return false;
        default:
            return true; //Process all other keycodes normally
    }
}
